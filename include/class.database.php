<?php
/**
 * Interact with the WPDK data tables.
 *
 * @property        wpdb    $wpdb
 * @property-read   string  $update_history_table;
 *
 * @package wpdkPlugin\Database
 * @author Lance Cleveland <lance@charlestonsw.com>
 * @copyright 2015 Charleston Software Associates, LLC
 */
class wpdkPlugin_Database extends WPDK_BaseClass_Object {
    public $wpdb;
    private $update_history_table;

    /**
     * Create and attach the admin UI object.
     */
    function initialize() {
        global $wpdb;
        $this->wpdb = $wpdb;
        $this->update_history_table = $wpdb->prefix . 'wpdk_update_history';
    }

    /**
     * Add an incoming request to the history table.
     */
    public function add_request_to_history() {
        $action =
            ( isset( $this->addon->UpdateEngine->request['fetch'] ) && ! empty( $this->addon->UpdateEngine->request['fetch'] ) ) ?
            $this->addon->UpdateEngine->request['fetch'] :
            'send_file'
            ;

        $request_value = maybe_serialize( $_REQUEST );

        $most_recent_request = $this->fetch_most_recent_matching_request( $request_value );

        // Insert new request
        if ( empty( $most_recent_request ) ) {
            $this->wpdb->insert(
                $this->update_history_table,
                array(
                    'action'          => $action,
                    'current_version' => $this->addon->UpdateEngine->request[ 'current_version' ],
                    'ip_address'      => $this->addon->UpdateEngine->get_request_ip_address(),
                    'new_version'     => $this->addon->UpdateEngine->current_plugin_meta[ $this->addon->current_target ][ 'new_version' ],
                    'sid'             => $this->addon->UpdateEngine->request[ 'sid' ],
                    'site_url'        => $this->addon->UpdateEngine->request[ 'surl' ],
                    'slug'            => $this->addon->current_plugin[ 'slug' ],
                    'plugin_meta'     => maybe_serialize( $this->addon->UpdateEngine->current_plugin_meta ),
                    'request'         => $request_value,
                    'server'          => maybe_serialize( $_SERVER ),
                    'target'          => $this->addon->current_target,
                    'uid'             => $this->addon->UpdateEngine->request[ 'uid' ],
                )
            );

        // Update existing request
        } else {
            $this->wpdb->update(
                $this->update_history_table,
                array(
                    'lastupdated' => current_time( 'mysql' )
                ),
                array( 'id' => $most_recent_request->id )
            );
        }
    }

    /**
     * Fetch ID of most recent request matching serialized $_REQUEST
     *
     * @param string $serialized_request
     * @return array|null|object|void
     */
    private function fetch_most_recent_matching_request( $serialized_request ) {
        $sql =
            $this->wpdb->prepare(
                'SELECT id FROM ' . $this->update_history_table . ' WHERE  request = %s ORDER BY lastupdated DESC LIMIT 1' ,
                $serialized_request
            );

        return $this->wpdb->get_row( $sql );
    }

    /**
     * Fetch the <n> most recent update requests.
     *
     * @param   int $limit
     * @return  array       array of update data
     */
    public function fetch_recent_history( $limit = 10 ) {
        return
            $this->wpdb->get_results(
                    'SELECT slug, current_version, target, new_version, site_url, lastupdated, action, uid, sid ' .
                    'FROM ' . $this->update_history_table . ' ' .
                    'ORDER BY lastupdated DESC ' .
                    sprintf( 'LIMIT %d' , $limit ),
                    ARRAY_A
                    );
    }

	/**
	 * Fetch the <n> most recent unique domains asking for update data.
	 *
	 * @param   int $limit
	 * @return  array       array of update data
	 */
	public function fetch_history_domains( $limit = 10 ) {
		return
			$this->wpdb->get_results(
				'SELECT DISTINCT site_url ' .
				'FROM ' . $this->update_history_table . ' ' .
				'ORDER BY lastupdated DESC ' .
				sprintf( 'LIMIT %d' , $limit ),
				ARRAY_A
			);
	}


}

