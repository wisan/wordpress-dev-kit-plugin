<?php
if (! class_exists('wpdkPlugin_Admin')) {

    /**
     * Holds the admin-only code.
     *
     * @property-read   wpdkPlugin_Admin_UI     $UI
     * @property=read   wpdkPlugin_Dashboard    $Dashboard
     * @property-read   string                  $menuHook
     *
     * @package wpdkPlugin\Admin
     * @author Lance Cleveland <lance@charlestonsw.com>
     * @copyright 2014 - 2016 Charleston Software Associates, LLC
     */
    class wpdkPlugin_Admin extends WPDK_BaseClass_Object {
        private $menuHook;
        private $Dashboard;
        private $UI;

        /**
         * Admin interface constructor.
         */
        function initialize() {
            $this->init_UI();
            $this->init_Dashboard();
            $this->menuHook = add_options_page( 'WP Dev Kit' , 'WP Dev Kit' , 'manage_options' , 'wpdevkit' , array( $this->UI , 'render_SettingsPage' ) );
        }

        /**
         * Create and attach the admin UI object.
         */
        private function init_UI() {
            if ( ! isset ( $this->UI  ) ) {
                require_once( 'class.admin.ui.php' );
                $this->UI = new wpdkPlugin_Admin_UI();
            }
        }
        
        /**
         * Add dashboard widgets.
         */
        private function init_Dashboard() {
            if ( ! isset ( $this->Dashboard  ) ) {
                require_once( 'class.admin.dashboard.php' );
                $this->Dashboard = new wpdkPlugin_Dashboard();
            }
        }
    }
}
