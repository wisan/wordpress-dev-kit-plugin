<?php
if ( ! class_exists('WPDK_REST_Handler') ) {

    /**
     * WP REST API interface.
     *
     * @package   wpdkPlugin\REST\Handler
     * @author    Lance Cleveland <lance@charlestonsw.com>
     * @copyright 2016 Charleston Software Associates, LLC
     *
     * text-domain: wp-dev-kit
     */
    class WPDK_REST_Handler extends WPDK_BaseClass_Object {

        /**
         * Initialize the REST handler.
         */
        function initialize() {
            if ( defined( 'REST_REQUEST' ) && REST_REQUEST ) {
                $this->setup_rest_endpoints();
            }
        }

	    /**
	     * Fetch the last <n> updates.
	     *
	     * Endpoint: /wp-json/wpdk/v1/updates/fetch/<n>
	     *
	     * @param WP_REST_Request $request
	     * @return string
	     */
	    function fetch_updates( WP_REST_Request $request ) {
		    $this->addon->create_object_Database();

		    $limit = max( $request['limit'] , 50 );
		    $update_history = $this->addon->Database->fetch_recent_history( $limit );

		    // No history
		    //
		    if ( is_null( $update_history ) ) {
			    return new WP_Error( 'wpdk_no_history' , __('No update history.', 'wp-dev-kit') , array( 'status' => 404 ) );
		    }

		    $response = new WP_REST_Response( $update_history );
		    $response->set_status( 201 );

		    return $response;
	    }

	    /**
	     * Fetch the last <n> domains from the history.
	     *
	     * Endpoint: /wp-json/wpdk/v1/history/fetch/domains/<n>
	     *
	     * @param WP_REST_Request $request
	     * @return string
	     */
	    function fetch_history_domains( WP_REST_Request $request ) {
		    $this->addon->create_object_Database();

		    if ( ! $this->request_ip_is_ok( $_SERVER['REMOTE_ADDR'] ) ) {
			    return new WP_Error( 'wpdk_invalid_ip_source' , sprintf( __("I'm sorry, %s. I'm afraid I can't do that.", 'wp-dev-kit') , $_SERVER['REMOTE_ADDR'] ) , array( 'status' => 404 ) );
		    }

		    $update_history = $this->addon->Database->fetch_history_domains( $request['limit'] );

		    // No history
		    //
		    if ( is_null( $update_history ) ) {
			    return new WP_Error( 'wpdk_no_history' , __('No update history.', 'wp-dev-kit') , array( 'status' => 404 ) );
		    }

		    $response = new WP_REST_Response( $update_history );
		    $response->set_status( 201 );

		    return $response;
	    }

        /**
         * Validate a user license.
         *
         * @param WP_REST_Request $request
         *
         * @return WP_REST_Response
         */
        public function license_validate( WP_REST_Request $request ) {
            $this->addon->create_object_Woo();
            $license_status = $this->addon->Woo->validate_subscription( $request['uid'] , $request['oid'] . '_' . $request['pid'] );

            if ( is_wp_error( $license_status ) ) {
                return $license_status;
            }

            if ( is_null( $license_status ) ) {
                return new WP_Error( 'wpdk_no_license' , __('No license.', 'wp-dev-kit') , array( 'status' => 404 ) );
            }

            if ( is_bool( $license_status ) && ! $license_status ) {
                $license_status = 'invalid';
            } else {
                $license_status = 'valid';
            }

            $response = new WP_REST_Response( $license_status );
            $response->set_status( 201 );

            return $response;
        }

	    /**
	     * Is requesting IP address from a valid source?
	     *
	     * @param string $request_ip
	     * @return bool
	     */
        private function request_ip_is_ok( $request_ip ) {
	        if ( empty( $request_ip ) ) {
		        return false;
	        }

	        $this->addon->set_options();

        	if ( empty( $this->addon->options['ip_filter_list'] ) ) {
        		return true;
	        }

	        $ips = explode( ',', $this->addon->options['ip_filter_list']);
	        if ( is_array( $ips ) ) {
		        $request_ip = ip2long( $request_ip );
		        foreach ( $ips as $ip ) {

		        	// Has * wildcard
			        if ( strpos( $ip1, '*' ) !== false ) {
				        $ip1 = str_replace( "*", "0", $ip );
				        $ip2 = str_replace( "*", "255", $ip );

				    // Specific IP
		            } else {
		            	$ip1 = $ip;
				        $ip2 = $ip;
			        }

			        if ( $request_ip >= ip2long( $ip1 ) && $request_ip <= ip2long( $ip2 ) ) {
				       return true;
			        }
		        }
	        }

        	return false;
        }

        /**
         * Create our endpoints list.
         */
        private function setup_rest_endpoints() {
        	$rest_root = 'wp-dev-kit/v1';

            register_rest_route( $rest_root,
                '/updates/fetch/(?P<limit>\d+)'                                                ,
                array( 'methods' => 'GET' , 'callback' => array( $this, 'fetch_updates'    ) )
            );

            register_rest_route( $rest_root ,
                '/license/validate/(?P<slug>[\w\-\_]+)/(?P<uid>\d+)/(?P<oid>\d+)_(?P<pid>\d+)' ,
                array( 'methods' => 'GET' , 'callback' => array( $this, 'license_validate' ) )
            );

	        register_rest_route( $rest_root ,
		        '/history/fetch/domains/(?P<limit>\d+)',
		        array( 'methods' => 'GET' , 'callback' => array( $this , 'fetch_history_domains' ) )
		        );
        }
    }

}